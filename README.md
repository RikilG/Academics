# Academics

## Table of Contents

 - [About this Repository](#about-this-repository)
 - [DSA](#dsa---data-structures-and-algorithms)
 - [DBMS](#dbms---database-management-system)
 - [SE](#se---software-engineering)
 - [OOPs](#oops---object-oriented-programming)
 - [OS](#os---operating-system)
 - [CA](#ca---computer-architecture)
 - [PoPL](#popl---principles-of-programming-languages)


## About This Repository

This repository contains all academic projects and assignments which i have done during my study at Birla Institute of Technology and Science, Pilani.
This reposository contains division based on courses


## DSA - Data Structures and Algorithms

Contains all lab sheets and their possible solutions written by me. Some might still be incorrect so be careful.


## DBMS - Database Management System

Contains few sql queries i have written for practice.


## SE - Software Engineering

This does not have its own folder as it is a big project and hence has a repository of its own.
Project: [Issue Redressal System](https://github.com/RikilG/household_issue_redressal_system)


## OOPs - Object Oriented Programming

Contains codes and practice done in OOPs concepts (including applets).  
Semester Project link: [Hotel-Booking-Portal](https://github.com/RikilG/Hotel-Booking-Portal)


## OS - Operating System

Contains codes and shell scripts written while studying OS.


## CA - Computer Architecture

Codes written in MIPS architecture.


## PoPL - Principles of Programming Languages

Functional and Logic programming techniques taught in class.